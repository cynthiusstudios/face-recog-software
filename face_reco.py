from PyQt5.QtWidgets import QApplication, QWidget, QVBoxLayout, QSlider, QLabel, QPushButton, QFileDialog, QGroupBox, QSplitter, QLineEdit
from PyQt5.QtWidgets import QSlider, QMessageBox, QDialog, QCheckBox, QHBoxLayout, QMainWindow, QTabWidget, QComboBox
from PyQt5.QtWidgets import QScrollArea, QRadioButton
import sys
from PyQt5 import QtCore, QtGui
import cv2
import face_recognition
import os
import datetime

if os.path.isdir("Predicted_Images") != True:
    os.mkdir("Predicted_Images")
    
order = []
test_order = []
known_faces = []
known_names = []
current_images = []
final_order = []

# class Window(QWidget):
#     def __init__(self):
#         super().__init__()
#         title = "Image Detection"
#         left = 410
#         top = 134
#         width = 1100
#         height = 812
#         self.setStyleSheet("QSplitter::handle { image: none; }")
#         self.setWindowTitle(title)
#         self.setGeometry(left, top, width, height)
#         self.setFixedSize(width,height)
        
#         vbox= QVBoxLayout()
        
#         tabWidget = QTabWidget()
#        # tabWidget.addTab(CNN(), "CNN")
#         tabWidget.addTab(Facial_Recog(), "Facial Recognition")
#         vbox.addWidget(tabWidget)
        
        # self.setLayout(vbox)        
        # self.show() 

# class CNN(QWidget):
    
#     def __init__(self):
#         super().__init__()
        
#         self.import_path = os.path.expanduser("~")
#         self.test_path = os.path.expanduser("~")
#         self.order = []
#         self.temp_directories =[]
#         self.existing_directories = []
#         self.final_order = []
#         self.name_list = []
#         self.final_names = []
#         self.test_order = []
#         self.test_directories = []
        
#         # final_names is the name for each pics. name_list is the array of each name 
        
#         vbox = QVBoxLayout()
#         vbox.setAlignment(QtCore.Qt.AlignTop)
        
#         # QSplitters
        
#         horizontal_split_1 = QSplitter(QtCore.Qt.Horizontal)
#         horizontal_split_1_1 = QSplitter(QtCore.Qt.Horizontal)
#         horizontal_split_1_2 = QSplitter(QtCore.Qt.Horizontal)
#         horizontal_split_2 = QSplitter(QtCore.Qt.Horizontal)
#         horizontal_split_3 = QSplitter(QtCore.Qt.Horizontal)
#         horizontal_split_4 = QSplitter(QtCore.Qt.Horizontal)
#         horizontal_split_4_0 = QSplitter(QtCore.Qt.Horizontal)
#         horizontal_split_5 = QSplitter(QtCore.Qt.Horizontal)
        
#         vertical_split_1_1 = QSplitter(QtCore.Qt.Vertical)
#         vertical_split_1 = QSplitter(QtCore.Qt.Vertical)
#         vertical_split_2 = QSplitter(QtCore.Qt.Vertical)
#         vertical_split_3 = QSplitter(QtCore.Qt.Vertical)
#         vertical_split_4 = QSplitter(QtCore.Qt.Vertical)
#         vertical_split_5 = QSplitter(QtCore.Qt.Vertical)
        
#         vertical_split_1.setFixedWidth(610)
#         vertical_split_1_1.setFixedWidth(520)
#         vertical_split_2.setFixedWidth(440)
#         vertical_split_3.setFixedWidth(440)
#         vertical_split_4.setFixedWidth(528)
#         vertical_split_5.setFixedWidth(522)
        
#         # Import Section
        
#         # Import Header Label
        
#         import_header = QLabel("<b>Import & Train</b>")
#         import_header.setAlignment(QtCore.Qt.AlignCenter)
#         vbox.addWidget(import_header)
        
#         # Import Selection Combo Box
        
#         self.import_selection_combo = QComboBox()
#         self.import_selection_combo.addItem("Select Directories")
#         self.import_selection_combo.addItem("Add Manually")
#         self.import_selection_combo.setFixedSize(262,30)
#         self.import_selection_combo.currentIndexChanged.connect(self.import_selection_combo_text)
#         horizontal_split_1.addWidget(self.import_selection_combo)
        
#         # Import Name Label
        
#         self.name = QLineEdit()
#         self.name.setFixedSize(262,30)
#         self.name.textChanged.connect(self.name_typed)
#         self.name.setPlaceholderText("Enter Label")
#         horizontal_split_1.addWidget(self.name)
        
        
#         vertical_split_1_1.addWidget(horizontal_split_1)
       
#         # Import Button 
       
#         self.import_button = QPushButton()
#         self.import_button.setText("Import the Directory")
#         self.import_button.setFixedSize(200,30)
#         self.import_button.clicked.connect(self.import_pressed)
#         horizontal_split_1_2.addWidget(self.import_button)
        
#         # Import Reset Button
        
#         self.import_reset = QPushButton("Reset")
#         self.import_reset.setFixedSize(160,30)
#         self.import_reset.setEnabled(False)
#         self.import_reset.clicked.connect(self.import_reset_pressed)
#         horizontal_split_1_2.addWidget(self.import_reset)
        
#         # Import Remove Last Button
        
#         self.import_remove_last = QPushButton("Remove Last")
#         self.import_remove_last.setFixedSize(160,30)
#         self.import_remove_last.setEnabled(False)
#         self.import_remove_last.clicked.connect(self.import_remove_last_pressed)
#         horizontal_split_1_2.addWidget(self.import_remove_last)
        
#         vertical_split_1_1.addWidget(horizontal_split_1_2)
#         horizontal_split_1_1.addWidget(vertical_split_1_1)
        
#         # Import Add Button
        
#         self.add_import_button = QPushButton("+")
#         self.add_import_button.setFixedSize(78,64)
#         self.add_import_button.setFont(QtGui.QFont("Sanserif",15))
#         self.add_import_button.clicked.connect(self.add_pressed)
#         self.add_import_button.setEnabled(False)
#         horizontal_split_1_1.addWidget(self.add_import_button)
        
#         vertical_split_1.addWidget(horizontal_split_1_1)
        
#         # Imported Directory / Image Scroll
        
#         import_scroll = QScrollArea()
#         import_scroll.setFixedSize(610,150)
#         import_scroll.setWidgetResizable(True)
#         self.import_label = QLabel()
#         import_scroll.setWidget(self.import_label)
#         vertical_split_1.addWidget(import_scroll)
        
#         # Import Label For Label Scroll
        
#         label_1 = QLabel("<i>Training Labels Imported:</i>")
#         label_1.setFixedHeight(25)
#         vertical_split_2.addWidget(label_1)
        
#         # Import Label Scroll
        
#         import_label_scroll = QScrollArea()
#         import_label_scroll.setFixedSize(440,189)
#         import_label_scroll.setWidgetResizable(True)
#         self.import_label_label = QLabel()
#         import_label_scroll.setWidget(self.import_label_label)
#         vertical_split_2.addWidget(import_label_scroll)
        
#         horizontal_split_2.addWidget(vertical_split_1)
#         horizontal_split_2.addWidget(vertical_split_2)
#         vbox.addWidget(horizontal_split_2)
        
#         # Node Input
        
#         self.node_input()
#         horizontal_split_3.addWidget(self.node_input_qbox)
        
#         # Node Label
        
#         node_label = QLabel("<i>Nodes Entered:</i>")
#         node_label.setFixedSize(440,19)
        
#         # Node Label Scroll
        
#         node_input_scroll = QScrollArea()
#         node_input_scroll.setFixedSize(440,181)
#         node_input_scroll.setWidgetResizable(True)
#         self.node_input_label = QLabel()
#         node_input_scroll.setWidget(self.node_input_label)
        
#         vertical_split_3.addWidget(node_label)
#         vertical_split_3.addWidget(node_input_scroll)
        
#         horizontal_split_3.addWidget(vertical_split_3)
        
#         vbox.addWidget(horizontal_split_3)
        
#         # Test Header
        
#         test_header = QLabel("<b>Import & Test</b>")
#         test_header.setAlignment(QtCore.Qt.AlignCenter)
#         vbox.addWidget(test_header)
        
#         # Test Selection Combo Box
        
#         self.test_selection_combo = QComboBox()
#         self.test_selection_combo.addItem("Select Directories")
#         self.test_selection_combo.addItem("Add Manually")
#         self.test_selection_combo.setFixedSize(528,30)
#         vertical_split_4.addWidget(self.test_selection_combo)
       
#         # Test Button  
       
#         self.test_button = QPushButton("Import the Directory")
#         self.test_button.setFixedSize(200,30)
#         self.test_button.clicked.connect(self.test_pressed)
#         horizontal_split_4_0.addWidget(self.test_button)
        
#         # Test Reset
        
#         self.test_reset = QPushButton("Reset")
#         self.test_reset.setFixedSize(160,30)
#         self.test_reset.setEnabled(False)
#         self.test_reset.clicked.connect(self.test_reset_clicked)
#         horizontal_split_4_0.addWidget(self.test_reset)
        
#         # Test Remove Last
        
#         self.test_remove_last = QPushButton("Remove Last")
#         self.test_remove_last.setFixedSize(160,30)
#         self.test_remove_last.setEnabled(False)
#         self.test_remove_last.clicked.connect(self.test_remove_last_clicked)
#         horizontal_split_4_0.addWidget(self.test_remove_last)
        
#         vertical_split_4.addWidget(horizontal_split_4_0)
        
#         # Test Directory / Image Label
        
#         test_scroll = QScrollArea()
#         test_scroll.setFixedSize(528,150)
#         test_scroll.setWidgetResizable(True)
#         self.test_label = QLabel()
#         test_scroll.setWidget(self.test_label)
#         vertical_split_4.addWidget(test_scroll)
        
#         # Requirements

#         self.requirements()        
#         horizontal_split_4.addWidget(vertical_split_4)
#         horizontal_split_4.addWidget(self.requirements_qbox)
#         vbox.addWidget(horizontal_split_4)
        
#         # Train Button
        
#         self.train_button = QPushButton("Train the Model")
#         self.train_button.setFixedHeight(30)
#         self.train_button.setEnabled(False)
#         vbox.addWidget(self.train_button)        
        
#         # Setting Layout to VBOX
        
#         self.setLayout(vbox)
    
#     # Node Input Box
    
#     def node_input(self):
#         self.node_input_qbox = QGroupBox("Select Inputs and Add Nodes:")
#         self.node_input_qbox.setFixedSize(610,204)
#         vbox = QVBoxLayout()
#         vbox.setAlignment(QtCore.Qt.AlignCenter)
#         hor_split_1 = QSplitter(QtCore.Qt.Horizontal)
        
#         input_node_button = QPushButton("Input Nodes")
#         input_node_button.clicked.connect(self.input_node_dialog)
#         input_node_button.setFixedSize(190,30)
        
#         hidden_node_button = QPushButton("Hidden Layer")
#         hidden_node_button.clicked.connect(self.hidden_node_dialog)
#         hidden_node_button.setFixedSize(190,30)
        
#         output_node_button = QPushButton("Output Nodes")
#         output_node_button.clicked.connect(self.output_node_dialog)
#         output_node_button.setFixedSize(190,30)
        
#         hor_split_1.addWidget(input_node_button)
#         hor_split_1.addWidget(hidden_node_button)
#         hor_split_1.addWidget(output_node_button)
        
#         vbox.addWidget(hor_split_1)
        
#         self.node_input_qbox.setLayout(vbox)
    
#     # Input Node Dialog
    
#     def input_node_dialog(self):
#         input_dial = QDialog()
#         input_dial.setGeometry(648,275,624,530)
#         input_dial.setFixedSize(624,530)
#         input_dial.setWindowTitle("Input Node Config")
#         input_dial.setStyleSheet("QSplitter::handle { image: none; }")
        
#         # Variables
#         self.input_conv_var = ''
#         self.input_nodes_var = 0
#         self.input_kernel_size_var = ''
#         self.input_usebias_var = False
#         self.input_padding_var = 'valid'
#         self.input_activation_var = 'None'
#         self.input_strides_var = ''
#         self.input_dilationrate_var = ''
#         self.input_kernel_initializer_var = 'glorot_uniform'
#         self.input_bias_initializer_var = 'zeros'
#         self.input_kernel_regularizer_var = 'None'
#         self.input_bias_regularizer_var = 'None'
#         self.input_kernel_constraint_var = 'None'
#         self.input_bias_constraint_var = 'None'
#         self.input_poolsize_var = ''
#         self.input_mp_strides_var = ''
#         self.input_flatten_var = False
#         self.input_height_var = 0
#         self.input_width_var = 0
#         self.input_grayscale_var = False
        
#         # Layouts | Splitters | Group Boxes
#         vbox = QVBoxLayout()
#         vbox_2 = QVBoxLayout()
#         vbox_3 = QVBoxLayout()
#         vbox_4 = QVBoxLayout()
#         hbox = QHBoxLayout()
#         qbox = QGroupBox("Convolution Type")
#         qbox_2 = QGroupBox("Tweak Parameters")
#         qbox_3 = QGroupBox("MaxPooling Parameters")
#         qbox_4 = QGroupBox("Additional Parameters")
#         horizontal_splitter_1 = QSplitter(QtCore.Qt.Horizontal)
#         horizontal_splitter_2 = QSplitter(QtCore.Qt.Horizontal)
#         horizontal_splitter_3 = QSplitter(QtCore.Qt.Horizontal)
#         horizontal_splitter_4 = QSplitter(QtCore.Qt.Horizontal)
#         horizontal_splitter_5 = QSplitter(QtCore.Qt.Horizontal)
#         horizontal_splitter_6 = QSplitter(QtCore.Qt.Horizontal)
#         horizontal_splitter_7 = QSplitter(QtCore.Qt.Horizontal)
        
#         # Conv 1D 2D & 3D Buttons
#         self.input_radio_1d = QRadioButton("Convolution 1D")
#         self.input_radio_2d = QRadioButton("Convolution 2D")
#         self.input_radio_3d = QRadioButton("Convolution 3D")
#         self.input_radio_dense = QRadioButton("Dense")
#         hbox.addWidget(self.input_radio_1d)
#         hbox.addWidget(self.input_radio_2d)
#         hbox.addWidget(self.input_radio_3d)
#         hbox.addWidget(self.input_radio_dense)
        
#         # Adding qbox to VBOX
#         qbox.setLayout(hbox)
#         qbox.setFixedHeight(70)
#         vbox.addWidget(qbox)
        
#         # Number of Nodes 
#         self.input_node_number = QLineEdit()
#         self.input_node_number.setPlaceholderText("Nodes/Units")
#         self.input_node_number.setValidator(QtGui.QIntValidator())
#         self.input_node_number.setFixedSize(130,30)
#         horizontal_splitter_1.addWidget(self.input_node_number)
        
#         # Kernel Size
#         self.input_kernel_size = QLineEdit()
#         self.input_kernel_size.setPlaceholderText("Kernel Size")
#         self.input_kernel_size.setValidator(QtGui.QIntValidator())
#         self.input_kernel_size.setFixedSize(85,30)
#         horizontal_splitter_1.addWidget(self.input_kernel_size)
        
#         # Use Bias
#         input_use_bias_label = QLabel(" Use Bias?")
#         input_use_bias_label.setFixedSize(70,30)
#         self.input_usebias = QCheckBox()
#         self.input_usebias.setFixedSize(20,30)
#         self.input_usebias.setStyleSheet("QCheckBox::indicator { width: 18px; height: 18px;}")
#         horizontal_splitter_1.addWidget(input_use_bias_label)
#         horizontal_splitter_1.addWidget(self.input_usebias)
        
#         # Padding
#         input_padding = QLabel("Padding:")
#         input_padding.setFixedSize(60,30)
#         self.input_padding_combo = QComboBox()
#         self.input_padding_combo.addItem("valid")
#         self.input_padding_combo.addItem("same")
#         self.input_padding_combo.setFixedSize(190,30)
#         horizontal_splitter_1.addWidget(input_padding)
#         horizontal_splitter_1.addWidget(self.input_padding_combo)
    
#         # Activation
#         input_activation_label = QLabel("       Activation:")
#         input_activation_label.setFixedSize(95,30)
#         input_activation_list = ["None","deserialize", "elu", "get", "exponential", "hard_sigmoid", "linear", "relu", 
#                                  "selu", "serialize", "sigmoid", "softmax", "softplus", "softsign", "swish", "tanh"]
#         self.input_activation = QComboBox()
#         for i in range(0, len(input_activation_list)):
#             self.input_activation.addItem(input_activation_list[i])
#         self.input_activation.setFixedSize(150,30)
#         horizontal_splitter_2.addWidget(input_activation_label)
#         horizontal_splitter_2.addWidget(self.input_activation)
       
#         # Strides
#         self.input_strides = QLineEdit()
#         self.input_strides.setPlaceholderText("Strides")
#         self.input_strides.setValidator(QtGui.QIntValidator())
#         self.input_strides.setFixedSize(85,30)
#         horizontal_splitter_2.addWidget(self.input_strides)
        
#         # Dialation Rate
#         self.input_dilation_rate = QLineEdit()
#         self.input_dilation_rate.setPlaceholderText("Dilation Rate")
#         self.input_dilation_rate.setValidator(QtGui.QDoubleValidator())
#         self.input_dilation_rate.setFixedSize(185,30)
#         horizontal_splitter_2.addWidget(self.input_dilation_rate)
        
#         # Kernel Initializer
#         input_kernel_initializer_label = QLabel(" Kernel Initializer:")
#         input_kernel_initializer_label.setFixedSize(120,30)
#         input_kernel_initializer_list = ["Constant", "GlorotNormal", "GlorotUniform", "Identity", "Initializer",
#                                          "Ones", "Orthogonal", "RandomNormal", "TruncatedNormal", "VarianceScaling",
#                                          "Zeros","deserialize","get", "he_normal", "he_uniform", "lecum_normal", "lecum_uniform", "serialize"]
#         self.input_kernel_initializer = QComboBox()
#         for i in range(0, len(input_kernel_initializer_list)):
#             self.input_kernel_initializer.addItem(input_kernel_initializer_list[i])
#         self.input_kernel_initializer.setFixedSize(150,30)
#         horizontal_splitter_3.addWidget(input_kernel_initializer_label)
#         horizontal_splitter_3.addWidget(self.input_kernel_initializer)
        
#         # Bias Initializer
#         input_bias_initializer_label = QLabel("    Bias Initializer:")
#         input_bias_initializer_label.setFixedSize(120,30)
#         input_bias_initializer_list = ["Constant", "GlorotNormal", "GlorotUniform", "Identity", "Initializer",
#                                          "Ones", "Orthogonal", "RandomNormal", "TruncatedNormal", "VarianceScaling",
#                                          "Zeros","deserialize","get", "he_normal", "he_uniform", "lecum_normal", "lecum_uniform", "serialize"]
#         self.input_bias_initializer = QComboBox()
#         for i in range(0, len(input_bias_initializer_list)):         
#             self.input_bias_initializer.addItem(input_bias_initializer_list[i])

#         self.input_bias_initializer.setFixedSize(150,30)
#         horizontal_splitter_3.addWidget(input_bias_initializer_label)
#         horizontal_splitter_3.addWidget(self.input_bias_initializer)
        
#         # Kernel Regularizer
#         input_kernel_regularizer_label = QLabel(" Kernel Regularizer:")
#         input_kernel_regularizer_label.setFixedSize(130,30)
#         input_kernel_regularizer_list = ["None","L1L2", "Regularizer", "deserialize", "get", "l1", "l1_l2", "serialize", "l2"]
#         self.input_kernel_regularizer = QComboBox()
#         for i in range(0, len(input_kernel_regularizer_list)):
#             self.input_kernel_regularizer.addItem(input_kernel_regularizer_list[i])
#         self.input_kernel_regularizer.setFixedSize(140,30)
#         horizontal_splitter_4.addWidget(input_kernel_regularizer_label)
#         horizontal_splitter_4.addWidget(self.input_kernel_regularizer)
        
#         # Bias Regularizer
#         input_bias_regularizer_label = QLabel("    Bias Regularizer:")
#         input_bias_regularizer_label.setFixedSize(130,30)
#         input_bias_regularizer_list = ["None","L1L2", "Regularizer", "deserialize", "get", "l1", "l1_l2", "serialize", "l2"]        
#         self.input_bias_regularizer = QComboBox()
#         for i in range(0, len(input_bias_regularizer_list)):
#             self.input_bias_regularizer.addItem(input_bias_regularizer_list[i])
#         self.input_bias_regularizer.setFixedSize(140,30)
#         horizontal_splitter_4.addWidget(input_bias_regularizer_label)
#         horizontal_splitter_4.addWidget(self.input_bias_regularizer)
        
#         # Kernel Constraint
#         input_kernel_constraint_label = QLabel(" Kernel Constraint:")
#         input_kernel_constraint_label.setFixedSize(130,30)
#         input_kernel_constraint_list = ["None","Constraint", "MaxNorm", "MinMaxNorm", "NonNeg", "RadialConstraint",
#                                         "UnitNorm", "deserialize", "get", "serialize"]
#         self.input_kernel_constraint = QComboBox()
#         for i in range(0, len(input_kernel_constraint_list)):
#             self.input_kernel_constraint.addItem(input_kernel_constraint_list[i])
#         self.input_kernel_constraint.setFixedSize(140,30)
#         horizontal_splitter_5.addWidget(input_kernel_constraint_label)
#         horizontal_splitter_5.addWidget(self.input_kernel_constraint)
        
#         # Bias Constraint
#         input_bias_constraint_label = QLabel("    Bias Regularizer:")
#         input_bias_constraint_label.setFixedSize(130,30)
#         input_bias_constraint_list = ["None","Constraint", "MaxNorm", "MinMaxNorm", "NonNeg", "RadialConstraint",
#                                       "UnitNorm", "deserialize", "get", "serialize"]
#         self.input_bias_constraint = QComboBox()
#         for i in range(0, len(input_bias_constraint_list)):
#             self.input_bias_constraint.addItem(input_bias_constraint_list[i])
#         self.input_bias_constraint.setFixedSize(140,30)
#         horizontal_splitter_5.addWidget(input_bias_constraint_label)
#         horizontal_splitter_5.addWidget(self.input_bias_constraint)
        
#         # Adding qbox_2 to VBOX
#         vbox_2.addWidget(horizontal_splitter_1)
#         vbox_2.addWidget(horizontal_splitter_2)
#         vbox_2.addWidget(horizontal_splitter_3)
#         vbox_2.addWidget(horizontal_splitter_4)
#         vbox_2.addWidget(horizontal_splitter_5)
#         vbox_2.setAlignment(QtCore.Qt.AlignTop)
#         qbox_2.setLayout(vbox_2)
#         vbox.addWidget(qbox_2)
        
#         # Max Pooling
#         # Pool Size
#         self.input_pool_size = QLineEdit()
#         self.input_pool_size.setPlaceholderText("Pool Size")
#         self.input_pool_size.setValidator(QtGui.QDoubleValidator())
#         self.input_pool_size.setFixedSize(85,30)
#         horizontal_splitter_6.addWidget(self.input_pool_size)
        
#         # Max Pool Strides
#         self.input_mp_strides = QLineEdit()
#         self.input_mp_strides.setPlaceholderText("Strides")
#         self.input_mp_strides.setValidator(QtGui.QIntValidator())
#         self.input_mp_strides.setFixedSize(85,30)
#         horizontal_splitter_6.addWidget(self.input_mp_strides) 
        
#         # Max Pooling Padding
#         input_mp_padding = QLabel("Padding:")
#         input_mp_padding.setFixedSize(60,30)
#         self.input_mp_padding_combo = QComboBox()
#         self.input_mp_padding_combo.addItem("valid")
#         self.input_mp_padding_combo.addItem("same")
#         self.input_mp_padding_combo.setFixedSize(190,30)
#         horizontal_splitter_6.addWidget(input_mp_padding)
#         horizontal_splitter_6.addWidget(self.input_mp_padding_combo)
        
#         # Adding qbox_3 to VBOX
#         vbox_3.addWidget(horizontal_splitter_6)
#         qbox_3.setLayout(vbox_3)
#         vbox.addWidget(qbox_3)
        
#         # Additional Params
#         # Dropout
#         self.input_dropout = QLineEdit()
#         self.input_dropout.setPlaceholderText("Dropout")
#         self.input_dropout.setValidator(QtGui.QDoubleValidator())
#         self.input_dropout.setFixedSize(85,30)
#         horizontal_splitter_7.addWidget(self.input_dropout)
        
#         # Flatten
#         input_flatten_label = QLabel(" Flatten?")
#         input_flatten_label.setFixedSize(60,30)
#         self.input_flatten = QCheckBox()
#         self.input_flatten.setFixedSize(20,30)
#         self.input_flatten.setStyleSheet("QCheckBox::indicator { width: 18px; height: 18px;}")
#         horizontal_splitter_7.addWidget(input_flatten_label)
#         horizontal_splitter_7.addWidget(self.input_flatten)
        
#         # Height
#         self.input_height = QLineEdit()
#         self.input_height.setPlaceholderText("Height")
#         self.input_height.setValidator(QtGui.QDoubleValidator())
#         self.input_height.setFixedSize(85,30)
#         horizontal_splitter_7.addWidget(self.input_height)
        
#         # Width
#         self.input_height = QLineEdit()
#         self.input_height.setPlaceholderText("Width")
#         self.input_height.setValidator(QtGui.QDoubleValidator())
#         self.input_height.setFixedSize(85,30)
#         horizontal_splitter_7.addWidget(self.input_height)   
        
#         # Convert to GrayScale?
#         input_grayscale_label = QLabel(" Convert To Grayscale?")
#         input_grayscale_label.setFixedSize(155,30)
#         self.input_grayscale = QCheckBox()
#         self.input_grayscale.setFixedSize(20,30)
#         self.input_grayscale.setStyleSheet("QCheckBox::indicator { width: 18px; height: 18px;}")
#         horizontal_splitter_7.addWidget(input_grayscale_label)
#         horizontal_splitter_7.addWidget(self.input_grayscale)
        
#         # Addiving qbox_4 to VBOX
#         vbox_4.addWidget(horizontal_splitter_7)
#         qbox_4.setLayout(vbox_4)
#         vbox.addWidget(qbox_4)
                
#         # Save Button
#         save_input = QPushButton("Save")
#         vbox.addWidget(save_input)
        
#         # Setting input_dial layout to VBOX & Executing it
#         input_dial.setLayout(vbox)
#         input_dial.exec_()
    
#     # Hidden Node Dialog
    
#     def hidden_node_dialog(self):
#         hidden_dial = QDialog()
#         hidden_dial.setGeometry(648,260,624,560)
#         hidden_dial.setFixedSize(624,560)
#         hidden_dial.setWindowTitle("Add Hidden Layers")
#         hidden_dial.setStyleSheet("QSplitter::handle { image: none; }")
        
#          # Variables
#         self.hidden_conv_var = ''
#         self.hidden_nodes_var = 0
#         self.hidden_kernel_size_var = ''
#         self.hidden_usebias_var = False
#         self.hidden_padding_var = 'valid'
#         self.hidden_activation_var = 'None'
#         self.hidden_strides_var = ''
#         self.hidden_dilationrate_var = ''
#         self.hidden_kernel_initializer_var = 'glorot_uniform'
#         self.hidden_bias_initializer_var = 'zeros'
#         self.hidden_kernel_regularizer_var = 'None'
#         self.hidden_bias_regularizer_var = 'None'
#         self.hidden_kernel_constraint_var = 'None'
#         self.hidden_bias_constraint_var = 'None'
#         self.hidden_poolsize_var = ''
#         self.hidden_mp_strides_var = ''
#         self.hidden_flatten_var = False
        
#         # Layouts | Splitters | Group Boxes
#         vbox = QVBoxLayout()
#         vbox_2 = QVBoxLayout()
#         vbox_3 = QVBoxLayout()
#         vbox_4 = QVBoxLayout()
#         hbox = QHBoxLayout()
#         qbox = QGroupBox("Convolution Type")
#         qbox_2 = QGroupBox("Tweak Parameters")
#         qbox_3 = QGroupBox("MaxPooling Parameters")
#         qbox_4 = QGroupBox("Additional Parameters")
#         horizontal_splitter_1 = QSplitter(QtCore.Qt.Horizontal)
#         horizontal_splitter_2 = QSplitter(QtCore.Qt.Horizontal)
#         horizontal_splitter_3 = QSplitter(QtCore.Qt.Horizontal)
#         horizontal_splitter_4 = QSplitter(QtCore.Qt.Horizontal)
#         horizontal_splitter_5 = QSplitter(QtCore.Qt.Horizontal)
#         horizontal_splitter_6 = QSplitter(QtCore.Qt.Horizontal)
#         horizontal_splitter_7 = QSplitter(QtCore.Qt.Horizontal)
        
#         # Conv 1D 2D & 3D Buttons
#         self.hidden_radio_1d = QRadioButton("Convolution 1D")
#         self.hidden_radio_2d = QRadioButton("Convolution 2D")
#         self.hidden_radio_3d = QRadioButton("Convolution 3D")
#         self.hidden_radio_dense = QRadioButton("Dense")
#         hbox.addWidget(self.hidden_radio_1d)
#         hbox.addWidget(self.hidden_radio_2d)
#         hbox.addWidget(self.hidden_radio_3d)
#         hbox.addWidget(self.hidden_radio_dense)
        
#         # Adding qbox to VBOX
#         qbox.setLayout(hbox)
#         qbox.setFixedHeight(70)
#         vbox.addWidget(qbox)
        
#         # Number of Nodes 
#         self.hidden_node_number = QLineEdit()
#         self.hidden_node_number.setPlaceholderText("Number of Nodes")
#         self.hidden_node_number.setValidator(QtGui.QIntValidator())
#         self.hidden_node_number.setFixedSize(130,30)
#         horizontal_splitter_1.addWidget(self.hidden_node_number)
        
#         # Kernel Size
#         self.hidden_kernel_size = QLineEdit()
#         self.hidden_kernel_size.setPlaceholderText("Kernel Size")
#         self.hidden_kernel_size.setValidator(QtGui.QIntValidator())
#         self.hidden_kernel_size.setFixedSize(85,30)
#         horizontal_splitter_1.addWidget(self.hidden_kernel_size)
        
#         # Use Bias
#         hidden_use_bias_label = QLabel(" Use Bias?")
#         hidden_use_bias_label.setFixedSize(70,30)
#         self.hidden_usebias = QCheckBox()
#         self.hidden_usebias.setFixedSize(20,30)
#         self.hidden_usebias.setStyleSheet("QCheckBox::indicator { width: 18px; height: 18px;}")
#         horizontal_splitter_1.addWidget(hidden_use_bias_label)
#         horizontal_splitter_1.addWidget(self.hidden_usebias)
        
#         # Padding
#         hidden_padding = QLabel("Padding:")
#         hidden_padding.setFixedSize(60,30)
#         self.hidden_padding_combo = QComboBox()
#         self.hidden_padding_combo.addItem("valid")
#         self.hidden_padding_combo.addItem("same")
#         self.hidden_padding_combo.setFixedSize(190,30)
#         horizontal_splitter_1.addWidget(hidden_padding)
#         horizontal_splitter_1.addWidget(self.hidden_padding_combo)
    
#         # Activation
#         hidden_activation_label = QLabel("       Activation:")
#         hidden_activation_label.setFixedSize(95,30)
#         hidden_activation_list = ["deserialize", "elu", "get", "exponential", "hard_sigmoid", "linear", "relu", 
#                                  "selu", "serialize", "sigmoid", "softmax", "softplus", "softsign", "swish", "tanh"]
#         self.hidden_activation = QComboBox()
#         for i in range(0, len(hidden_activation_list)):
#             self.hidden_activation.addItem(hidden_activation_list[i])
#         self.hidden_activation.setFixedSize(150,30)
#         horizontal_splitter_2.addWidget(hidden_activation_label)
#         horizontal_splitter_2.addWidget(self.hidden_activation)
       
#         # Strides
#         self.hidden_strides = QLineEdit()
#         self.hidden_strides.setPlaceholderText("Strides")
#         self.hidden_strides.setValidator(QtGui.QIntValidator())
#         self.hidden_strides.setFixedSize(85,30)
#         horizontal_splitter_2.addWidget(self.hidden_strides)
        
#         # Dialation Rate
#         self.hidden_dilation_rate = QLineEdit()
#         self.hidden_dilation_rate.setPlaceholderText("Dilation Rate")
#         self.hidden_dilation_rate.setValidator(QtGui.QDoubleValidator())
#         self.hidden_dilation_rate.setFixedSize(185,30)
#         horizontal_splitter_2.addWidget(self.hidden_dilation_rate)
        
#         # Kernel Initializer
#         hidden_kernel_initializer_label = QLabel(" Kernel Initializer:")
#         hidden_kernel_initializer_label.setFixedSize(120,30)
#         hidden_kernel_initializer_list = ["Constant", "GlorotNormal", "GlorotUniform", "Identity", "Initializer",
#                                          "Ones", "Orthogonal", "RandomNormal", "TruncatedNormal", "VarianceScaling",
#                                          "Zeros","deserialize","get", "he_normal", "he_uniform", "lecum_normal", "lecum_uniform", "serialize"]
#         self.hidden_kernel_initializer = QComboBox()
#         for i in range(0, len(hidden_kernel_initializer_list)):
#             self.hidden_kernel_initializer.addItem(hidden_kernel_initializer_list[i])
#         self.hidden_kernel_initializer.setFixedSize(150,30)
#         horizontal_splitter_3.addWidget(hidden_kernel_initializer_label)
#         horizontal_splitter_3.addWidget(self.hidden_kernel_initializer)
        
#         # Bias Initializer
#         hidden_bias_initializer_label = QLabel("    Bias Initializer:")
#         hidden_bias_initializer_label.setFixedSize(120,30)
#         hidden_bias_initializer_list = ["Constant", "GlorotNormal", "GlorotUniform", "Identity", "Initializer",
#                                          "Ones", "Orthogonal", "RandomNormal", "TruncatedNormal", "VarianceScaling",
#                                          "Zeros","deserialize","get", "he_normal", "he_uniform", "lecum_normal", "lecum_uniform", "serialize"]
#         self.hidden_bias_initializer = QComboBox()
#         for i in range(0, len(hidden_bias_initializer_list)):
#             self.hidden_bias_initializer.addItem(hidden_bias_initializer_list[i])
#         self.hidden_bias_initializer.setFixedSize(150,30)
#         horizontal_splitter_3.addWidget(hidden_bias_initializer_label)
#         horizontal_splitter_3.addWidget(self.hidden_bias_initializer)
        
#         # Kernel Regularizer
#         hidden_kernel_regularizer_label = QLabel(" Kernel Regularizer:")
#         hidden_kernel_regularizer_label.setFixedSize(130,30)
#         hidden_kernel_regularizer_list = ["L1L2", "Regularizer", "deserialize", "get", "l1", "l1_l2", "serialize", "l2"]
#         self.hidden_kernel_regularizer = QComboBox()
#         for i in range(0, len(hidden_kernel_regularizer_list)):
#             self.hidden_kernel_regularizer.addItem(hidden_kernel_regularizer_list[i])
#         self.hidden_kernel_regularizer.setFixedSize(140,30)
#         horizontal_splitter_4.addWidget(hidden_kernel_regularizer_label)
#         horizontal_splitter_4.addWidget(self.hidden_kernel_regularizer)
        
#         # Bias Regularizer
#         hidden_bias_regularizer_label = QLabel("    Bias Regularizer:")
#         hidden_bias_regularizer_label.setFixedSize(130,30)
#         hidden_bias_regularizer_list = ["L1L2", "Regularizer", "deserialize", "get", "l1", "l1_l2", "serialize", "l2"]
#         self.hidden_bias_regularizer = QComboBox()
#         for i in range(0, len(hidden_bias_regularizer_list)):
#             self.hidden_bias_regularizer.addItem(hidden_bias_regularizer_list[i])
#         self.hidden_bias_regularizer.setFixedSize(140,30)
#         horizontal_splitter_4.addWidget(hidden_bias_regularizer_label)
#         horizontal_splitter_4.addWidget(self.hidden_bias_regularizer)
        
#         # Kernel Constraint
#         hidden_kernel_constraint_label = QLabel(" Kernel Constraint:")
#         hidden_kernel_constraint_label.setFixedSize(130,30)
#         hidden_kernel_constraint_list = ["Constraint", "MaxNorm", "MinMaxNorm", "NonNeg", "RadialConstraint",
#                                         "UnitNorm", "deserialize", "get", "serialize"]
#         self.hidden_kernel_constraint = QComboBox()
#         for i in range(0, len(hidden_kernel_constraint_list)):
#             self.hidden_kernel_constraint.addItem(hidden_kernel_constraint_list[i])
#         self.hidden_kernel_constraint.setFixedSize(140,30)
#         horizontal_splitter_5.addWidget(hidden_kernel_constraint_label)
#         horizontal_splitter_5.addWidget(self.hidden_kernel_constraint)
        
#         # Bias Constraint
#         hidden_bias_constraint_label = QLabel("    Bias Regularizer:")
#         hidden_bias_constraint_label.setFixedSize(130,30)
#         hidden_bias_constraint_list = ["Constraint", "MaxNorm", "MinMaxNorm", "NonNeg", "RadialConstraint",
#                                         "UnitNorm", "deserialize", "get", "serialize"]
#         self.hidden_bias_constraint = QComboBox()
#         for i in range(0, len(hidden_bias_constraint_list)):
#             self.hidden_bias_constraint.addItem(hidden_bias_constraint_list[i])
#         self.hidden_bias_constraint.setFixedSize(140,30)
#         horizontal_splitter_5.addWidget(hidden_bias_constraint_label)
#         horizontal_splitter_5.addWidget(self.hidden_bias_constraint)
                
#         # Adding qbox_2 to VBOX
#         vbox_2.addWidget(horizontal_splitter_1)
#         vbox_2.addWidget(horizontal_splitter_2)
#         vbox_2.addWidget(horizontal_splitter_3)
#         vbox_2.addWidget(horizontal_splitter_4)
#         vbox_2.addWidget(horizontal_splitter_5)
#         vbox_2.setAlignment(QtCore.Qt.AlignTop)
#         qbox_2.setLayout(vbox_2)
#         vbox.addWidget(qbox_2)
        
#         # MaxPooling
#         # Pool Size
#         self.hidden_pool_size = QLineEdit()
#         self.hidden_pool_size.setPlaceholderText("Pool Size")
#         self.hidden_pool_size.setValidator(QtGui.QIntValidator())
#         self.hidden_pool_size.setFixedSize(85,30)
#         horizontal_splitter_6.addWidget(self.hidden_pool_size)
        
#         # Strides
#         self.hidden_mp_strides = QLineEdit()
#         self.hidden_mp_strides.setPlaceholderText("Strides")
#         self.hidden_mp_strides.setValidator(QtGui.QIntValidator())
#         self.hidden_mp_strides.setFixedSize(85,30)
#         horizontal_splitter_6.addWidget(self.hidden_mp_strides)
        
#         # Padding
#         hidden_mp_padding = QLabel("Padding:")
#         hidden_mp_padding.setFixedSize(60,30)
#         self.hidden_mp_padding_combo = QComboBox()
#         self.hidden_mp_padding_combo.addItem("valid")
#         self.hidden_mp_padding_combo.addItem("same")
#         self.hidden_mp_padding_combo.setFixedSize(190,30)
#         horizontal_splitter_6.addWidget(hidden_mp_padding)
#         horizontal_splitter_6.addWidget(self.hidden_mp_padding_combo)
        
#         # Adding qbox_3 to VBOX
#         vbox_3.addWidget(horizontal_splitter_6)
#         qbox_3.setLayout(vbox_3)
#         vbox.addWidget(qbox_3)
        
#         # Dropout
#         self.hidden_dropout = QLineEdit()
#         self.hidden_dropout.setPlaceholderText("Dropout")
#         self.hidden_dropout.setValidator(QtGui.QDoubleValidator())
#         self.hidden_dropout.setFixedSize(85,30)
#         horizontal_splitter_7.addWidget(self.hidden_dropout)
        
#         # Flatten
#         hidden_flatten_label = QLabel(" Flatten?")
#         hidden_flatten_label.setFixedSize(60,30)
#         self.hidden_flatten = QCheckBox()
#         self.hidden_flatten.setFixedSize(20,30)
#         self.hidden_flatten.setStyleSheet("QCheckBox::indicator { width: 18px; height: 18px;}")
#         horizontal_splitter_7.addWidget(hidden_flatten_label)
#         horizontal_splitter_7.addWidget(self.hidden_flatten)
        
#         # Adding qbox_4 to VBOX
#         vbox_4.addWidget(horizontal_splitter_7)
#         qbox_4.setLayout(vbox_4)
#         vbox.addWidget(qbox_4)
        
#         # Add button
#         add_hidden = QPushButton("+")
#         vbox.addWidget(add_hidden)
        
#         # Save Button
#         save_hidden = QPushButton("Save")
#         vbox.addWidget(save_hidden)
        
#         # Setting input_dial layout to VBOX & Executing it
#         hidden_dial.setLayout(vbox)
#         hidden_dial.exec_()
    
                
#     def output_node_dialog(self):
#         output_dial = QDialog()
#         output_dial.setGeometry(648,385,624,330)
#         output_dial.setFixedSize(624,330)
#         output_dial.setStyleSheet("QSplitter::handle { image: none; }")      
        
#         # Variables
#         self.output_nodes_var = 0
#         self.output_kernel_size_var = ''
#         self.output_activation_var = 'None'
#         self.output_kernel_initializer_var = 'glorot_uniform'
#         self.output_bias_initializer_var = 'zeros'
#         self.output_kernel_regularizer_var = 'None'
#         self.output_bias_regularizer_var = 'None'
#         self.output_kernel_constraint_var = 'None'
#         self.output_bias_constraint_var = 'None'
#         self.output_poolsize_var = ''
#         self.output_mp_strides_var = ''
#         self.output_flatten_var = False
        
#         # Layouts | Splitters | Group Boxes
#         vbox = QVBoxLayout()
#         vbox_2 = QVBoxLayout()
#         vbox_3 = QVBoxLayout()
#         qbox = QGroupBox("Tweak Parameters")
#         qbox_2 = QGroupBox("Additional Parameters")
#         horizontal_splitter_1 = QSplitter(QtCore.Qt.Horizontal)
#         horizontal_splitter_2 = QSplitter(QtCore.Qt.Horizontal)
#         horizontal_splitter_3 = QSplitter(QtCore.Qt.Horizontal)
#         horizontal_splitter_4 = QSplitter(QtCore.Qt.Horizontal)
#         horizontal_splitter_5 = QSplitter(QtCore.Qt.Horizontal)

#         # Number of Nodes 
#         self.output_node_number = QLineEdit()
#         self.output_node_number.setPlaceholderText("Number of Nodes")
#         self.output_node_number.setValidator(QtGui.QIntValidator())
#         self.output_node_number.setFixedSize(130,30)
#         horizontal_splitter_1.addWidget(self.output_node_number)
        
    
#         # Activation
#         output_activation_label = QLabel("    Activation:")
#         output_activation_label.setFixedSize(95,30)
#         output_activation_list = ["None","deserialize", "elu", "get", "exponential", "hard_sigmoid", "linear", "relu", 
#                                  "selu", "serialize", "sigmoid", "softmax", "softplus", "softsign", "swish", "tanh"]
#         self.output_activation = QComboBox()
#         for i in range(0, len(output_activation_list)):
#             self.output_activation.addItem(output_activation_list[i])
#         self.output_activation.setFixedSize(120,30)
#         horizontal_splitter_1.addWidget(output_activation_label)
#         horizontal_splitter_1.addWidget(self.output_activation)
        
#         # Kernel Initializer
#         output_kernel_initializer_label = QLabel(" Kernel Initializer:")
#         output_kernel_initializer_label.setFixedSize(120,30)
#         output_kernel_initializer_list = ["Constant", "GlorotNormal", "GlorotUniform", "Identity", "Initializer",
#                                          "Ones", "Orthogonal", "RandomNormal", "TruncatedNormal", "VarianceScaling",
#                                          "Zeros","deserialize","get", "he_normal", "he_uniform", "lecum_normal", "lecum_uniform", "serialize"]
#         self.output_kernel_initializer = QComboBox()
#         for i in range(0, len(output_kernel_initializer_list)):
#             self.output_kernel_initializer.addItem(output_kernel_initializer_list[i])
#         self.output_kernel_initializer.setFixedSize(150,30)
#         horizontal_splitter_2.addWidget(output_kernel_initializer_label)
#         horizontal_splitter_2.addWidget(self.output_kernel_initializer)
        
#         # Bias Initializer
#         output_bias_initializer_label = QLabel(" Bias Initializer:")
#         output_bias_initializer_label.setFixedSize(120,30)
#         output_bias_initializer_list = ["Constant", "GlorotNormal", "GlorotUniform", "Identity", "Initializer",
#                                          "Ones", "Orthogonal", "RandomNormal", "TruncatedNormal", "VarianceScaling",
#                                          "Zeros","deserialize","get", "he_normal", "he_uniform", "lecum_normal", "lecum_uniform", "serialize"]
#         self.output_bias_initializer = QComboBox()
#         for i in range(0, len(output_bias_initializer_list)):
#             self.output_bias_initializer.addItem(output_bias_initializer_list[i])
#         self.output_bias_initializer.setFixedSize(150,30)
#         horizontal_splitter_2.addWidget(output_bias_initializer_label)
#         horizontal_splitter_2.addWidget(self.output_bias_initializer)
        
#         # Kernel Regularizer
#         output_kernel_regularizer_label = QLabel(" Kernel Regularizer:")
#         output_kernel_regularizer_label.setFixedSize(130,30)
#         output_kernel_regularizer_list = ["None","L1L2", "Regularizer", "deserialize", "get", "l1", "l1_l2", "serialize", "l2"]
#         self.output_kernel_regularizer = QComboBox()
#         for i in range(0, len(output_kernel_regularizer_list)):
#             self.output_kernel_regularizer.addItem(output_kernel_regularizer_list[i])
#         self.output_kernel_regularizer.setFixedSize(140,30)
#         horizontal_splitter_3.addWidget(output_kernel_regularizer_label)
#         horizontal_splitter_3.addWidget(self.output_kernel_regularizer)
        
#         # Bias Regularizer
#         output_bias_regularizer_label = QLabel("    Bias Regularizer:")
#         output_bias_regularizer_label.setFixedSize(130,30)
#         output_bias_regularizer_list = ["None","L1L2", "Regularizer", "deserialize", "get", "l1", "l1_l2", "serialize", "l2"]
#         self.output_bias_regularizer = QComboBox()
#         for i in range(0, len(output_bias_regularizer_list)):
#             self.output_bias_regularizer.addItem(output_bias_regularizer_list[i])
#         self.output_bias_regularizer.setFixedSize(140,30)
#         horizontal_splitter_3.addWidget(output_bias_regularizer_label)
#         horizontal_splitter_3.addWidget(self.output_bias_regularizer)
        
#         # Kernel Constraint
#         output_kernel_constraint_label = QLabel(" Kernel Constraint:")
#         output_kernel_constraint_label.setFixedSize(130,30)
#         output_kernel_constraint_list = ["None","Constraint", "MaxNorm", "MinMaxNorm", "NonNeg", "RadialConstraint",
#                                         "UnitNorm", "deserialize", "get", "serialize"]
#         self.output_kernel_constraint = QComboBox()
#         for i in range(0, len(output_kernel_constraint_list)):
#             self.output_kernel_constraint.addItem(output_kernel_constraint_list[i])
#         self.output_kernel_constraint.setFixedSize(140,30)
#         horizontal_splitter_4.addWidget(output_kernel_constraint_label)
#         horizontal_splitter_4.addWidget(self.output_kernel_constraint)
        
#         # Bias Constraint
#         output_bias_constraint_label = QLabel("    Bias Regularizer:")
#         output_bias_constraint_label.setFixedSize(130,30)
#         output_bias_constraint_list = ["None","Constraint", "MaxNorm", "MinMaxNorm", "NonNeg", "RadialConstraint",
#                                         "UnitNorm", "deserialize", "get", "serialize"]
#         self.output_bias_constraint = QComboBox()
#         for i in range(0, len(output_bias_constraint_list)):
#             self.output_bias_constraint.addItem(output_bias_constraint_list[i])
#         self.output_bias_constraint.setFixedSize(140,30)
#         horizontal_splitter_4.addWidget(output_bias_constraint_label)
#         horizontal_splitter_4.addWidget(self.output_bias_constraint)
        
#         # Adding qbox_2 to vbox_2
#         vbox_2.addWidget(horizontal_splitter_1)
#         vbox_2.addWidget(horizontal_splitter_2)
#         vbox_2.addWidget(horizontal_splitter_3)
#         vbox_2.addWidget(horizontal_splitter_4)
#         vbox_2.addWidget(horizontal_splitter_5)
#         qbox.setLayout(vbox_2)
#         vbox.addWidget(qbox)
        
#         # Additional Parameters
#         # Dropout
#         self.output_dropout = QLineEdit()
#         self.output_dropout.setPlaceholderText("Dropout")
#         self.output_dropout.setValidator(QtGui.QDoubleValidator())
#         self.output_dropout.setFixedSize(85,30)
#         horizontal_splitter_5.addWidget(self.output_dropout)
        
#         # Flatten
#         output_flatten_label = QLabel(" Flatten?")
#         output_flatten_label.setFixedSize(60,30)
#         self.output_flatten = QCheckBox()
#         self.output_flatten.setFixedSize(20,30)
#         self.output_flatten.setStyleSheet("QCheckBox::indicator { width: 18px; height: 18px;}")
#         horizontal_splitter_5.addWidget(output_flatten_label)
#         horizontal_splitter_5.addWidget(self.output_flatten)
        
#         # Adding qbox_2 to VBOX
#         vbox_3.addWidget(horizontal_splitter_5)
#         qbox_2.setLayout(vbox_3)
#         vbox.addWidget(qbox_2)
        
#         # Save Button
#         save_hidden = QPushButton("Save")
#         vbox.addWidget(save_hidden)
        
#         output_dial.setLayout(vbox)
#         output_dial.exec()
    
#     def node_save_btn(self):
#         sender = self.sender()
        
    
#     def requirements(self):
#         self.requirements_qbox = QGroupBox("Requirements:")
#         self.requirements_qbox.setFixedSize(524,218)
#         vbox = QVBoxLayout()
        
#         self.requirements_qbox.setLayout(vbox)
    
#     def import_selection_combo_text(self):
#         if self.import_selection_combo.currentText() == "Select Directories":
#             self.import_button.setText("Import the Directory")
#         else:
#             self.import_button.setText("Import Images")
    
#     def name_typed(self):
#         if self.name.text() != "" and self.name.text().isspace() == False and self.name.text() not in self.name_list and len(self.order) != 0:
#             self.add_import_button.setEnabled(True)
#         elif self.name.text() != "" and self.name.text().isspace() == False and self.name.text() not in self.name_list and len(self.temp_directories) != 0:
#             self.add_import_button.setEnabled(True)
#         else:
#             self.add_import_button.setEnabled(False)
    
#     def import_pressed(self):
#         if self.import_selection_combo.currentText() == "Select Directories":
#             temp = 0
#             fname = QFileDialog.getExistingDirectory(self, "Select Directory", self.import_path)
#             if(fname != ""):
#                 if(fname not in self.existing_directories and fname not in self.temp_directories):
#                     for file in os.listdir(fname):
#                         if file.endswith(('.png', '.jpg', '.jpeg')):
#                             if os.path.join(fname,file) not in self.final_order:
#                                     temp +=1
#                             else:
#                                 er_box = QMessageBox()
#                                 er_box.setWindowTitle("Image Skipped")
#                                 er_box.setText(f"{file} Already Exists! We Skipped Adding It!")
#                                 er_box.setIcon(QMessageBox.Warning)
#                                 er_box.exec_()
                        
                        
#                     if temp == 0:
#                         er_box = QMessageBox()
#                         er_box.setWindowTitle("No Image Files Found")
#                         er_box.setText("We Couldn't find any Image Files in the Directory! Please Enter Images with the suitable format!")
#                         er_box.setIcon(QMessageBox.Warning)
#                         er_box.exec_()
#                     else:
#                         self.import_selection_combo.setEnabled(False)
#                         self.temp_directories.append(fname)
#                         self.import_reset.setEnabled(True)
#                         self.import_remove_last.setEnabled(True)
#                         if self.name.text() != "" and self.name.text().isspace() == False and self.name.text() not in self.name_list:
#                             self.add_import_button.setEnabled(True)
#                         self.import_label.setText(self.import_label.text() + fname + "\n")
#                         x = fname.split('/')
#                         x.pop(len(x)-1)
#                         self.import_name = '/'.join(x)
#                 else:
#                     er_box = QMessageBox()
#                     er_box.setWindowTitle("Directory Already Exists")
#                     er_box.setText("Directory You Added Already Exists! Please Enter Another Directory!")
#                     er_box.setIcon(QMessageBox.Warning)
#                     er_box.exec_()

#         else:
#             fname = QFileDialog.getOpenFileNames(self, "Select Images", self.import_path, 'Image Files (*.jpg *.png *.jpeg)')
#             if(fname[0] != ""):
#                 for name in fname[0]:
#                     x = name.split('/')
#                     x.pop(len(x)-1)
#                     self.import_path = '/'.join(x)
#                     if(name not in self.order and name not in self.final_order and self.import_path not in self.existing_directories):
#                         self.import_selection_combo.setEnabled(False)
#                         self.order.append(name)
#                         self.import_remove_last.setEnabled(True)
#                         self.import_reset.setEnabled(True)
#                         self.import_label.setText(self.import_label.text() + name + "\n")
#                         if self.name.text() != "" and self.name.text().isspace() == False and self.name.text() not in self.name_list:
#                             self.add_import_button.setEnabled(True)
#                     else:
#                         er_box = QMessageBox()
#                         er_box.setWindowTitle("Image Already Exists")
#                         er_box.setText(f"{name} You Added Already Exists! Please Enter Another Image!")
#                         er_box.setIcon(QMessageBox.Warning)
#                         er_box.exec_()

    
#     def import_reset_pressed(self):
#         self.import_reset.setEnabled(False)
#         self.import_remove_last.setEnabled(False)
#         self.add_import_button.setEnabled(False)
#         self.import_label.setText("")
#         self.import_label_label.setText("")
#         self.order.clear()
#         self.name_list.clear()
#         self.final_order.clear()
#         self.temp_directories.clear()
#         self.existing_directories.clear()
#         self.name.setText("")
#         self.import_selection_combo.setEnabled(True)
        
#     def import_remove_last_pressed(self):
#         if self.import_selection_combo.currentText() == "Select Directories":
#             self.temp_directories.pop(len(self.temp_directories)-1)
#             self.import_label.setText("")
#             for i in range(0, len(self.temp_directories)):
#                 self.import_label.setText(self.import_label.text() + f"{self.temp_directories[i]}\n" )
#             if len(self.temp_directories) == 0:
#                 self.add_import_button.setEnabled(False)
#                 self.import_label.setText("")
#                 self.import_selection_combo.setEnabled(True)
#                 self.import_remove_last.setEnabled(False)
#                 if(len(final_order) == 0):
#                     self.import_reset.setEnabled(False)
#         else:
#             self.order.pop(len(self.order)-1)
#             self.import_label.setText("")
#             for i in range(0,len(self.order)):
#                 self.import_label.setText(self.import_label.text() + f"{self.order[i]}\n")
#             if len(self.order) == 0:
#                 self.add_import_button.setEnabled(False)
#                 self.import_label.setText("")
#                 self.import_selection_combo.setEnabled(True)
#                 self.import_remove_last.setEnabled(False)
#                 if(len(self.final_order) == 0):
#                     self.import_reset.setEnabled(False)
        
#     def add_pressed(self):
#         if self.import_selection_combo.currentText() == "Select Directories":
#             self.import_label_label.setText(self.import_label_label.text() + "\n" + self.name.text() + "\n")
            
#             for directory in self.temp_directories:
#                 self.import_label_label.setText(self.import_label_label.text() + directory + "\n")
#                 self.existing_directories.append(directory)
#                 for file in os.listdir(directory):
#                     if(file.endswith(('.png', '.jpg', '.jpeg')) and os.path.join(directory, file) not in self.final_order):
#                         self.final_order.append(os.path.join(directory,file))
#                         self.final_names.append(self.name.text())
                
#             self.name_list.append(self.name.text())
#             self.temp_directories.clear()
#             self.import_selection_combo.setEnabled(True)
#             self.add_import_button.setEnabled(False)
#             self.name.setText("")
#             self.import_remove_last.setEnabled(False)
#             self.import_label.setText("")
#             self.import_path = os.path.expanduser("~")
#         else:
#             self.import_label_label.setText(self.import_label_label.text() + "\n" + self.name.text() + "\n")
            
#             for files in self.order:
#                 x = files.split('/')
#                 x = x[len(x)-1]
#                 self.import_label_label.setText(self.import_label_label.text() + x + "\n")
#                 self.final_order.append(files)
#                 self.final_names.append(self.name.text())
                
                
#             self.name_list.append(self.name.text())
#             self.order.clear()
#             self.import_selection_combo.setEnabled(True)
#             self.add_import_button.setEnabled(False)
#             self.name.setText("")
#             self.import_remove_last.setEnabled(False)
#             self.import_label.setText("")
#             self.import_path = os.path.expanduser("~")
        
#     def test_pressed(self):
#         if self.test_selection_combo.currentText() == "Select Directories":
#             temp = 0
#             fname = QFileDialog.getExistingDirectory(self, "Select Directory", self.test_path)
#             if fname != "":
#                 if fname not in self.test_directories:
#                     for file in os.listdir(fname):
#                         if file.endswith(('.png', '.jpg', '.jpeg')):
#                             if os.path.join(fname,file) not in self.test_order:
#                                 temp +=1                                
                                
#                             else:
#                                 er_box = QMessageBox()
#                                 er_box.setWindowTitle("Image Skipped")
#                                 er_box.setText(f"{file} Already Exists! We Skipped Adding It!")
#                                 er_box.setIcon(QMessageBox.Warning)
#                                 er_box.exec_()
                     
#                     if temp == 0:
#                         er_box = QMessageBox()
#                         er_box.setWindowTitle("No Image Files Found")
#                         er_box.setText("We Couldn't find any Image Files in the Directory! Please Enter Images with the suitable format!")
#                         er_box.setIcon(QMessageBox.Warning)
#                         er_box.exec_()
                
#                     else:
#                         self.test_directories.append(fname)
#                         self.test_selection_combo.setEnabled(False)
#                         self.test_reset.setEnabled(True)
#                         self.test_remove_last.setEnabled(True)
#                         self.train_button.setEnabled(True)
#                         self.test_label.setText(self.test_label.text() + fname + "\n")
#                         x = fname.split('/')
#                         x.pop(len(x)-1)
#                         self.test_path = '/'.join(x)
#                 else:
#                     er_box = QMessageBox()
#                     er_box.setWindowTitle("Directory Already Exists")
#                     er_box.setText("Directory You Added Already Exists! Please Enter Another Directory!")
#                     er_box.setIcon(QMessageBox.Warning)
#                     er_box.exec_()
        
#         else:
#             fname = QFileDialog.getOpenFileNames(self, "Select Images", self.test_path, 'Image Files (*.jpg *.png *.jpeg)')
#             if(fname[0] != ""):
#                 for name in fname[0]:
#                     x = name.split('/')
#                     x.pop(len(x)-1)
#                     self.test_path = '/'.join(x)
#                     if(name not in self.test_order and self.test_path not in self.test_directories):
#                         self.test_selection_combo.setEnabled(False)
#                         self.test_order.append(name)
#                         self.test_remove_last.setEnabled(True)
#                         self.test_reset.setEnabled(True)
#                         self.test_label.setText(self.test_label.text() + name + "\n")
#                         self.train_button.setEnabled(True)
                        
#                     else:
#                         er_box = QMessageBox()
#                         er_box.setWindowTitle("Image Already Exists")
#                         er_box.setText(f"{name} You Added Already Exists! Please Enter Another Image!")
#                         er_box.setIcon(QMessageBox.Warning)
#                         er_box.exec_()
        
#     def test_reset_clicked(self):
#         self.test_reset.setEnabled(False)
#         self.test_remove_last.setEnabled(False)
#         self.train_button.setEnabled(False)
#         self.test_label.setText("")
#         self.test_order.clear()
#         self.test_directories.clear()
#         self.test_selection_combo.setEnabled(True)
    
#     def test_remove_last_clicked(self):
#         if self.test_selection_combo.currentText() == "Select Directories":
#             self.test_directories.pop(len(self.test_directories)-1)
#             self.test_label.setText("")
#             for i in range(0, len(self.test_directories)):
#                 self.test_label.setText(self.test_label.text() + f"{self.test_directories[i]}\n" )
#             if len(self.test_directories) == 0:
#                 self.test_label.setText("")
#                 self.test_selection_combo.setEnabled(True)
#                 self.test_reset.setEnabled(False)
#                 self.train_button.setEnabled(False)
#                 self.test_remove_last.setEnabled(False)
                
#         else:
#             self.test_order.pop(len(self.order)-1)
#             self.test_label.setText("")
#             for i in range(0,len(self.order)):
#                 self.test_label.setText(self.test_label.text() + f"{self.test_order[i]}\n")
#             if len(self.test_order) == 0:
#                 self.test_label.setText("")
#                 self.test_selection_combo.setEnabled(True)
#                 self.test_reset.setEnabled(False)
#                 self.train_button.setEnabled(False)
#                 self.test_remove_last.setEnabled(False)
#     def train_button_clicked(self):
#         pass
    
class Facial_Recog(QWidget):
    def __init__(self):
        super().__init__()
        
        title = "Image Detection"
        left = 410
        top = 134
        width = 1100
        height = 812
        self.setStyleSheet("QSplitter::handle { image: none; }")
        self.setWindowTitle(title)
        self.setGeometry(left, top, width, height)
        self.setFixedSize(width,height)
        
        vbox = QVBoxLayout()
        
        self.import_path = os.path.expanduser("~")
        self.test_path = os.path.expanduser("~")
        vbox = QVBoxLayout()
        
        vertical_layout_split_1 = QSplitter(QtCore.Qt.Vertical)
        vertical_layout_split_2 = QSplitter(QtCore.Qt.Vertical)

        horizontal_layout_split = QSplitter(QtCore.Qt.Horizontal)
        horizontal_layout_split_2 = QSplitter(QtCore.Qt.Horizontal)
        
        vertical_layout_split_1.setFixedWidth(598)
        vertical_layout_split_2.setFixedWidth(250)
        self.gpu = "hog"
        
        
        temp_label = QLabel("Import Images")
        temp_label.setFixedSize(650,20)
        temp_label.setAlignment(QtCore.Qt.AlignCenter)
        vertical_layout_split_1.addWidget(temp_label)
        
        self.name = QLineEdit()
        self.name.setPlaceholderText("Enter Name of the Person in the Image")
        self.name.textChanged.connect(self.name_typed)
        self.name.setFixedSize(598,30)
        vertical_layout_split_1.addWidget(self.name)
        
        self.import_button = QPushButton("Import Your Images So Model Can Learn How You Look :D", self)
        self.import_button.clicked.connect(self.import_button_clicked)
        self.import_button.setFixedSize(598,30)
        vertical_layout_split_1.addWidget(self.import_button)
        
        qsplit = QSplitter(QtCore.Qt.Horizontal)
        
        self.resetButton = QPushButton("Reset", self)
        self.resetButton.setEnabled(False)
        self.resetButton.clicked.connect(self.reset) 
        self.resetButton.setFixedSize(298,30)
        qsplit.addWidget(self.resetButton)
        
        self.removeLastButton = QPushButton("Remove Last", self)
        self.removeLastButton.setEnabled(False)
        self.removeLastButton.setFixedSize(297,30)
        self.removeLastButton.clicked.connect(self.removeLast)
        qsplit.addWidget(self.removeLastButton)
        
        vertical_layout_split_1.addWidget(qsplit)
        
        training_import_scroll = QScrollArea()
        training_import_scroll.setFixedSize(598,235)
        training_import_scroll.setWidgetResizable(True)
        self.import_label = QLabel()
        training_import_scroll.setWidget(self.import_label)
        vertical_layout_split_1.addWidget(training_import_scroll)
        
        self.addButton = QPushButton("+")
        self.addButton.setFixedSize(598,30)
        self.addButton.setEnabled(False)
        self.addButton.clicked.connect(self.add_button_clicked)
        vertical_layout_split_1.addWidget(self.addButton)
        
        self.test_button = QPushButton("Import Images of Yourself and Others and See The Model Predict Who You Are:")
        self.test_button.clicked.connect(self.test_button_clicked)
        self.test_button.setFixedSize(598,30)
        vertical_layout_split_1.addWidget(self.test_button)
        
        q_testsplit = QSplitter(QtCore.Qt.Horizontal)
        
        self.testresetButton = QPushButton("Reset", self)
        self.testresetButton.setEnabled(False)
        self.testresetButton.clicked.connect(self.test_reset) 
        self.testresetButton.setFixedSize(298,30)
        q_testsplit.addWidget(self.testresetButton)
        
        self.testremoveLastButton = QPushButton("Remove Last", self)
        self.testremoveLastButton.setEnabled(False)
        self.testremoveLastButton.setFixedSize(297,30)
        self.testremoveLastButton.clicked.connect(self.test_remove_last)
        q_testsplit.addWidget(self.testremoveLastButton)
        
        vertical_layout_split_1.addWidget(q_testsplit)
        
        test_images_scroll = QScrollArea()
        test_images_scroll.setFixedSize(598,238)
        test_images_scroll.setWidgetResizable(True)
        self.test_label = QLabel(self)
        test_images_scroll.setWidget(self.test_label)
        vertical_layout_split_1.addWidget(test_images_scroll)
        
        
        self.todo_list()
        self.additional_settings()
        vertical_layout_split_2.addWidget(self.additional_qbox)
        
        added_names_label = QLabel("<i>Added Labels & Images:</i>")
        added_names_label.setMaximumSize(250,30)
        vertical_layout_split_2.addWidget(added_names_label)
        
        added_names_scroll = QScrollArea()
        added_names_scroll.setFixedSize(250,310)
        added_names_scroll.setWidgetResizable(True)
        self.added_names_label = QLabel()
        added_names_scroll.setWidget(self.added_names_label)
        vertical_layout_split_2.addWidget(added_names_scroll)
        
        horizontal_layout_split.addWidget(self.todo_qbox)
        horizontal_layout_split.addWidget(vertical_layout_split_1)
        horizontal_layout_split.addWidget(vertical_layout_split_2)
        vbox.addWidget(horizontal_layout_split)
        
        self.trainModelButton = QPushButton("Train The Model ( The Software May Freeze till the Model Trains)", self)
        self.trainModelButton.setEnabled(False)
        self.trainModelButton.setFixedSize(802,28)
        self.trainModelButton.clicked.connect(self.train_the_model)
        
        self.videoModelButton = QPushButton("Video Input For Training")
        self.videoModelButton.setEnabled(False)
        self.videoModelButton.clicked.connect(self.video_train_button)
        
        horizontal_layout_split_2.addWidget(self.trainModelButton)
        horizontal_layout_split_2.addWidget(self.videoModelButton)
        
        vbox.addWidget(horizontal_layout_split_2)
        self.setLayout(vbox)
        self.show()      
        
    def todo_list(self):
        self.todo_qbox = QGroupBox("Requirements")
        self.todo_qbox.setAlignment(QtCore.Qt.AlignCenter)
        vbox = QVBoxLayout()
        vbox.setAlignment(QtCore.Qt.AlignCenter)
        self.todo_qbox.setFixedWidth(200)
        
        todo_name = QSplitter(QtCore.Qt.Horizontal)
        self.check_name = QCheckBox()
        self.check_name.setEnabled(False)
        self.todo_name_label = QLabel(" Set Name\t\t")
        todo_name.addWidget(self.check_name)
        todo_name.addWidget(self.todo_name_label)
        vbox.addWidget(todo_name)
        
        todo_import = QSplitter(QtCore.Qt.Horizontal)
        self.check_import = QCheckBox()
        self.check_import.setEnabled(False)
        self.todo_import_label = QLabel(" Imported Train Image")
        todo_import.addWidget(self.check_import)
        todo_import.addWidget(self.todo_import_label)
        vbox.addWidget(todo_import)
        
        todo_test = QSplitter(QtCore.Qt.Horizontal)
        self.check_test = QCheckBox()
        self.check_test.setEnabled(False)
        self.todo_test_label = QLabel(" Imported Test Image")
        todo_test.addWidget(self.check_test)
        todo_test.addWidget(self.todo_test_label)
        vbox.addWidget(todo_test)
        
        self.todo_qbox.setLayout(vbox)
        
    def import_button_clicked(self):
        fname = QFileDialog.getOpenFileNames(self, 'Open File', self.import_path, 'Image files (*.jpg *.png *.jpeg)')
        if("" not in fname[0]):  
            for name in fname[0]:
                x = name.split('/')
                x.pop(len(x) -1)
                self.import_path = str('/'.join(x))
                if(name not in order):
                    self.import_label.setText(self.import_label.text() + f"{name}\n")
                    self.removeLastButton.setEnabled(True)
                    self.resetButton.setEnabled(True)
                    order.append(name)
                    
                    x = name.split('/')
                    x.pop(len(x) -1)
                    self.import_path = str('/'.join(x))
                   
                    if(len(order) != 0 and self.name.text() != "" and self.name.text().isspace() is False):
                        self.addButton.setEnabled(True)
                    else:
                        self.addButton.setEnabled(False)
                else:
                    exist_box = QMessageBox()
                    exist_box.setWindowTitle("Error")
                    exist_box.setText(f"{name} already exists.")
                    exist_box.setIcon(QMessageBox.Warning)
                    exist_box.exec_()
                
        
    def reset(self):
        order.clear()
        final_order.clear()
        known_names.clear()
        self.import_label.setText("")
        self.trainModelButton.setEnabled(False)
        self.resetButton.setEnabled(False)
        self.removeLastButton.setEnabled(False)
        self.check_import.setChecked(False)
        self.addButton.setEnabled(False)
        self.added_names_label.setText("")
        self.name.setText("")
        self.videoModelButton.setEnabled(False)
        self.check_name.setChecked(False)
        
    def removeLast(self):
        order.pop(len(order)-1)
        self.import_label.setText("")
        for i in range(0, len(order)):
            self.import_label.setText(self.import_label.text() + f"{order[i]}\n")
        if(len(order) == 0):
            self.removeLastButton.setEnabled(False)
            self.addButton.setEnabled(False)
            if(len(final_order) == 0):
                self.resetButton.setEnabled(False)
                self.check_import.setChecked(False)
                self.check_name.setChecked(False)
                self.videoModelButton.setEnabled(False)
                    
    
    def name_typed(self):
     
        if(len(order) != 0 and self.name.text() != "" and self.name.text().isspace() is False):
            self.addButton.setEnabled(True)
        else:
            self.addButton.setEnabled(False)
            
    # Additional Settings        
    def additional_settings(self):
        self.additional_qbox = QGroupBox("Advanced Settings")
        self.additional_qbox.setAlignment(QtCore.Qt.AlignCenter)
        self.additional_qbox.setFixedSize(250,361)
        vbox = QVBoxLayout()
        vbox.setAlignment(QtCore.Qt.AlignTop)
        
        self.tolerance_value = 0.6
        
        self.tolerance_label = QLabel("Tolerance: 0.6")
        vbox.addWidget(self.tolerance_label)
        
        # Tolerance Slider
        self.slider = QSlider(QtCore.Qt.Horizontal)
        self.slider.setMinimum(1)
        self.slider.setMaximum(10)
        self.slider.setTickPosition(QSlider.TicksBelow)
        self.slider.setTickInterval(1)
        self.slider.setValue(self.tolerance_value*10)
        self.slider.valueChanged.connect(self.tolerance_slider_value)
        vbox.addWidget(self.slider)        
        
        # Use GPU?
        gpu_label = QLabel("Use GPU?  ")
        self.gpu_check = QCheckBox()
        self.gpu_check.toggled.connect(self.gpu_used)
        temp_label = QLabel("\t\t")
        gpu_split = QSplitter(QtCore.Qt.Horizontal)
        gpu_split.addWidget(gpu_label)
        gpu_split.addWidget(self.gpu_check)
        gpu_split.addWidget(temp_label)
        vbox.addWidget(gpu_split)
        
        # Input
        self.camera_input_stream = QComboBox()
        for i in range(0,6):
            self.camera_input_stream.addItem(str(i))
        vbox.addWidget(self.camera_input_stream)
        
        self.additional_qbox.setLayout(vbox)
        
    def tolerance_slider_value(self):
        self.tolerance_value = self.slider.value()/10
        self.tolerance_label.setText(f"Tolerance: {self.tolerance_value}")
    
    def gpu_used(self):
        check = self.sender()
        if(check.isChecked() == True):
            self.gpu = 'cnn'
        else:
            self.gpu = 'hog'
            
    def add_button_clicked(self):
        
        if len(test_order) != 0:
            self.trainModelButton.setEnabled(True)
        else:
            self.trainModelButton.setEnabled(False)
        
        self.check_name.setChecked(True)
        self.check_import.setChecked(True)
        self.import_path = os.path.expanduser("~")
        self.videoModelButton.setEnabled(True)
        
        if self.name.text() not in known_names:
            for i in range(0, len(order)):
                known_names.append(self.name.text())
            
            for i in range(0, len(order)):
                final_order.append(order[i])
            
            self.added_names_label.setText(self.added_names_label.text() + self.name.text() + ":\n")
            for i in range(0, len(order)):
                x = order[i].split('/')
                self.added_names_label.setText(self.added_names_label.text() + x[len(x)-1] + "\n")
            
            order.clear()
            self.name.setText("")
            self.import_label.setText("")
            self.removeLastButton.setEnabled(False)
            
        else:
            er_box = QMessageBox()
            er_box.setWindowTitle("Name Already Exists")
            er_box.setText(f"{self.name.text()} already exists! Please try another name!")
            er_box.setIcon(QMessageBox.Warning)
            er_box.exec_()

    def test_button_clicked(self):
       fname = QFileDialog.getOpenFileNames(self, 'Open File', self.test_path, 'Image files (*.jpg *.png *.jpeg)')
       
       if("" not in fname[0]):
           for name in fname[0]:
               x = name.split('/')
               x = x.pop(len(x)-1)
               self.test_path = str('/'.join(x))
               if(name not in test_order):
                   self.test_label.setText(self.test_label.text() + f"{name}\n")
                   self.testremoveLastButton.setEnabled(True)
                   self.testresetButton.setEnabled(True)
                   test_order.append(name)
                   
                   x = name.split('/')
                   x = x.pop(len(x)-1)
                   self.test_path = str('/'.join(x))
                   
                   self.check_test.setChecked(True)
                   if(len(final_order) != 0 and len(test_order) != 0 and len(known_names) !=0):
                       self.trainModelButton.setEnabled(True)
                   else:
                       self.trainModelButton.setEnabled(False)
               else:
                   exist_box = QMessageBox()
                   exist_box.setWindowTitle("Error")
                   exist_box.setText(f"{name} already exists.")
                   exist_box.setIcon(QMessageBox.Warning)
                   exist_box.exec_()
               
    
    def test_reset(self):
        test_order.clear()
        self.test_label.setText("")
        self.trainModelButton.setEnabled(False)
        self.testresetButton.setEnabled(False)
        self.testremoveLastButton.setEnabled(False)
        self.check_test.setChecked(False)
    
    def test_remove_last(self):
        test_order.pop(len(test_order)-1)
        self.test_label.setText("")
        for i in range(0, len(test_order)):
            self.test_label.setText(self.test_label.text() + f"{test_order[i]}\n")
        if(len(test_order) == 0):
            self.testresetButton.setEnabled(False)
            self.testremoveLastButton.setEnabled(False)
            self.trainModelButton.setEnabled(False)
            self.check_test.setChecked(False)
  
    def train_the_model(self):
        current_images.clear()
        counter = len(final_order)
        counter2 = len(test_order)                    
        
        # Resizing In Fixed Aspect Ratio for W & H
        for filename in final_order:
            image = face_recognition.load_image_file(f'{filename}')
            # Resizing Height
            if(image.shape[0] > 610):
                percent = 610/image.shape[0]
                hei = int(image.shape[0] * percent)
                wid = int(image.shape[1] * percent)
                image = cv2.resize(image, (wid,hei), interpolation = cv2.INTER_AREA)
            # Resizing Width
            if(image.shape[1]>480):
                percent = 480/image.shape[1]
                hei = int(image.shape[0] * percent)
                wid = int(image.shape[1] * percent)
                image = cv2.resize(image, (wid,hei), interpolation = cv2.INTER_AREA)
            
            # Checking if faces are found after resizing
            try:
                
                encoding = face_recognition.face_encodings(image)[0]
    
                known_faces.append(encoding)
            except:
                counter -=1
                
        # IF Faces are Found
        if(counter != 0):
            for filename in test_order:
                image = face_recognition.load_image_file(f'{filename}')
                if(image.shape[0] > 610):
                    percent = 610/image.shape[0]
                    hei = int(image.shape[0] * percent)
                    wid = int(image.shape[1] * percent)
                    image = cv2.resize(image, (wid,hei), interpolation = cv2.INTER_AREA)
                if(image.shape[1]>480):
                    percent = 480/image.shape[1]
                    hei = int(image.shape[0] * percent)
                    wid = int(image.shape[1] * percent)
                    image = cv2.resize(image, (wid,hei), interpolation = cv2.INTER_AREA)
                    
                try:
                  
                    locations = face_recognition.face_locations(image, model=self.gpu)
                    encodings = face_recognition.face_encodings(image, locations)
                    image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)
                    for face_encoding, face_location in zip(encodings, locations):
                
                        results = face_recognition.compare_faces(known_faces, face_encoding, self.tolerance_value)
                       
                        match = None
                        if True in results: 
                            match = known_names[results.index(True)]
                            top_left = (face_location[3], face_location[0])
                            bottom_right = (face_location[1], face_location[2])
                
                
                            cv2.rectangle(image, top_left, bottom_right, (0,0,158), 3)
                            top_left = (face_location[3], face_location[2])
                            bottom_right = (face_location[1], face_location[2] + 22)
                            cv2.rectangle(image, top_left, bottom_right, (0,0,158), cv2.FILLED)
                            
                            cv2.putText(image, match, (face_location[3] + 10, face_location[2] + 15), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (200, 200, 200), 2)
                    date_time = datetime.datetime.now()
                    current_images.append(f"Predicted_Images/Img_{date_time}.jpg") # Append names of current images to make the image viewer
                    cv2.imwrite(f"Predicted_Images/Img_{date_time}.jpg", image)
                except:
                    counter2 -=1
                    
            if(counter2 > 0):
                self.image_viewer()
                
                
            else:
                er_box = QMessageBox()
                er_box.setWindowTitle("No Faces Found")
                er_box.setText("We Couldn't find any faces for Prediction! Please Enter Images with faces or try Entering Other Images!")
                er_box.setIcon(QMessageBox.Warning)
                er_box.exec_()
                
        # If No Faces Found in Training       
        else:
            exist_box = QMessageBox()
            exist_box.setWindowTitle("No Faces Found")
            exist_box.setText("We Couldn't find any faces for Training the Model! Please Enter Images with faces or try Entering Other Images!")
            exist_box.setIcon(QMessageBox.Warning)
            exist_box.exec_()
        
    def image_viewer(self):
        self.dial = QDialog()
        self.dial.setWindowTitle("View Predicted Images")
        self.dial.setGeometry(720, 235,480,610)
        self.dial.setStyleSheet("QSplitter::handle { image: none; }")
        self.iterate = 0
        vbox = QVBoxLayout()
        
        self.predictImgLabel = QLabel()
        pixmap = QtGui.QPixmap(f"{current_images[0]}")
        self.predictImgLabel.setPixmap(pixmap)
        vbox.addWidget(self.predictImgLabel)
        
        split = QSplitter(QtCore.Qt.Horizontal)
        
        # Previous Image
        backImgButton = QPushButton("View Previous")
        backImgButton.clicked.connect(self.image_viewer_back_btn)
        backImgButton.setMinimumHeight(30)
        backImgButton.setMaximumHeight(30)
        if(len(current_images) ==1):
            backImgButton.setEnabled(False)
        
        # Next Image
        openImgButton = QPushButton("View Next")
        openImgButton.clicked.connect(self.image_viewer_btn)
        openImgButton.setMinimumHeight(30)
        openImgButton.setMaximumHeight(30)
        if(len(current_images)== 1):
            openImgButton.setEnabled(False)
            
        # Delete All Images    
        deleteImgButton = QPushButton("Delete All Images")
        deleteImgButton.clicked.connect(self.delete_images)
        deleteImgButton.setMinimumHeight(30)
        deleteImgButton.setMaximumHeight(30)
        
        split.addWidget(backImgButton)
        split.addWidget(openImgButton)
        split.addWidget(deleteImgButton)
        vbox.addWidget(split)
        
        
        self.dial.setLayout(vbox)
        self.dial.exec_()
    
    def image_viewer_back_btn(self):
        self.iterate -=1
        if self.iterate < 0:
            self.iterate = len(current_images) - 1
        pixmap = QtGui.QPixmap(f"{current_images[self.iterate]}")
        self.predictImgLabel.setPixmap(pixmap)
        
    def image_viewer_btn(self):
        self.iterate +=1
        if self.iterate > (len(current_images)-1):
            self.iterate = 0
        pixmap = QtGui.QPixmap(f"{current_images[self.iterate]}")
        self.predictImgLabel.setPixmap(pixmap)
        
    def delete_images(self):
        for filename in os.listdir("Predicted_Images"):
            if f"Predicted_Images/{filename}" in current_images:
                os.unlink(f"Predicted_Images/{filename}")
        self.dial.close()
    
    def video_train_button(self):
        current_images.clear()
        counter = len(final_order)
        video_capture_source = int(self.camera_input_stream.currentText())
        video = cv2.VideoCapture(video_capture_source)            
        
        if video is None or not video.isOpened():
            exist_box = QMessageBox()
            exist_box.setWindowTitle(f"Unable to Open Video Source {video_capture_source}")
            exist_box.setText("The Video Source you selected was unable to open, please select another one!")
            exist_box.setIcon(QMessageBox.Warning)
            exist_box.exec_()
            
        else:
            for filename in final_order:
                image = face_recognition.load_image_file(f'{filename}')
                if(image.shape[0] > 610):
                    percent = 610/image.shape[0]
                    hei = int(image.shape[0] * percent)
                    wid = int(image.shape[1] * percent)
                    image = cv2.resize(image, (wid,hei), interpolation = cv2.INTER_AREA)
                if(image.shape[1]>480):
                    percent = 480/image.shape[1]
                    hei = int(image.shape[0] * percent)
                    wid = int(image.shape[1] * percent)
                    image = cv2.resize(image, (wid,hei), interpolation = cv2.INTER_AREA)
                
                try:
                            
                    encoding = face_recognition.face_encodings(image)[0]
        
                    known_faces.append(encoding)
                except:
                    counter -=1
            
            if(counter != 0):
                while True:
                    ret, image = video.read()
                    locations = face_recognition.face_locations(image, model=self.gpu)
                    encodings = face_recognition.face_encodings(image, locations)
                    
                    for face_encoding, face_location in zip(encodings, locations):
                
                        results = face_recognition.compare_faces(known_faces, face_encoding, self.tolerance_value)
                       
                        match = None
                        if True in results: 
                            match = known_names[results.index(True)]
                            
                            top_left = (face_location[3], face_location[0])
                            bottom_right = (face_location[1], face_location[2])
                
                            cv2.rectangle(image, top_left, bottom_right, (0,0,158), 3)
                            top_left = (face_location[3], face_location[2])
                            bottom_right = (face_location[1], face_location[2] + 22)
                            cv2.rectangle(image, top_left, bottom_right, (0,0,158), cv2.FILLED)
                            
                            cv2.putText(image, match, (face_location[3] + 10, face_location[2] + 15), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (200, 200, 200), 2)
                    
    
                    cv2.imshow("Facial Recog", image)
                    if cv2.waitKey(1) & 0xFF == ord("q"):
                        break
                        
                
            else:
                exist_box = QMessageBox()
                exist_box.setWindowTitle("No Faces Found")
                exist_box.setText("We Couldn't find any faces for Training the Model! Please Enter Images with faces or try Entering Other Images!")
                exist_box.setIcon(QMessageBox.Warning)
                exist_box.exec_()
             
if __name__ == "__main__":
    App = QApplication(sys.argv)
    window = Facial_Recog()
    sys.exit(App.exec())